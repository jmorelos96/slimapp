<?php
define('__ROOT__', dirname(dirname(__FILE__))); 
// require_once(__ROOT__.'/jwt/src/JWT.php'); 


require(__ROOT__."/slimapp/Slim/Slim.php");

// obtener conexion
class Database{
    public static function getConnection()
{
    try{
        $db_username = "websa003_appmd3";
        $db_password = "25Qr3xkmq#tJ";
        $connection = new PDO("mysql:host=websa003.mysql.guardedhost.com:3306;dbname=websa003_appmd3", $db_username, $db_password);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
    return $connection;
}
 
}

// necesario para entrar a la clase Slim
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim(array(
    "MODE" => "development",
    "TEMPLATES.PATH" => "./templates"
));
// inicio de la app
 $app->get('/', function() use ($app) {
     echo '<h1>Aplicacion CRUD SlimFramework</h1>';
     // $app->redirect('http://localhost:90/slimapp/books.php');
     });

// recibe una lista de libros - GET
$app->get("/books/", function () use ($app) {
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("SELECT * FROM books");
        $dbh->execute();
        $books = $dbh->fetchAll();
        $app->response()->header("Content-Type", "application/json");
        echo json_encode($books);
    }catch(PDOException  $e){
        echo $e->getMessage();
    }
});
// recibe por id - GET
$app->get("/book/:id", function($id) use($app) {
   try{
        $connection = getConnection();
        $dbh = $connection->prepare("SELECT * FROM books WHERE id = ?");
        $dbh->bindParam(1, $id);
        $dbh->execute();
        $book = $dbh->fetch();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->body(json_encode($book));
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
});
// crear libros - POST
$app->post("/book/insertar/", function () use($app) {
    if(isset($_POST['submit'])){

    
            $title   =  $app->request->post('title');
            $author  =  $app->request->post('author');
            $summary =  $app->request->post('summary');
        }else{
            echo "error";
        }
      try{
            $connection = Database::getConnection();
            $dbh = $connection->prepare("INSERT INTO books VALUES(null, ?, ?, ?)");
            $dbh->bindParam(1,$title);
            $dbh->bindParam(2,$author);
            $dbh->bindParam(3,$summary);
            $dbh->execute();
            $bookId = $connection->lastInsertId();
            $connection = null;
            $app->response->headers->set("Content-type", "application/json");
            $app->response->body($bookId.' '.$title.' '.$author.' '.$summary.' Libro creado');
            $app->redirect('/app/slimapp/books.php');
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
});
$app->get("/book/insertar/", function() use($app){
    header("Location: books.php");
    echo "regresa";
});
// actualizamos libros - UPDATE
$app->post("/book/update/", function () use ($app) {
      if (isset($_POST['submit'])) {
         // if ( !empty($_GET['id'])) {
         //    $id  = $_REQUEST['id'];
         //    }

            $id         = $app->request->post('id');
            $title      = $app->request->post('title');
            $author     = $app->request->post('author');
            $summary    = $app->request->post('summary');
    }else{
            echo "error";
    }
    try{

        $connection = Database::getConnection();
        $dbh = $connection->prepare("UPDATE books SET title = ?, summary = ?, author = ? WHERE id = ?");
        $dbh->bindParam(1, $title);
        $dbh->bindParam(2, $summary);
        $dbh->bindParam(3, $author);
        $dbh->bindParam(4, $id);
        $dbh->execute();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->body(json_encode('Se ha actualizado el '.' #'.$id));
        $app->redirect('/app/slimapp/books.php');
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }

});
// Editar
$app->post("/edit", function () use($app) {
    if (isset($_POST['submit'])) {

        if ( !empty($app->request->get['id'])) {
        $id = $app->request->post('id');
    }

            // $id         = $app->request->post('id');
            $title      = $app->request->post('title');
            $author     = $app->request->post('author');
            $summary    = $app->request->post('summary');
    }else{
            echo "error";
    }
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("UPDATE books SET title = ?, summary = ?, author = ? WHERE id = ?");
        $dbh->bindParam(1, $title);
        $dbh->bindParam(2, $summary);
        $dbh->bindParam(3, $author);
        $dbh->bindParam(4, $id);
        $dbh->execute();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->redirect("/slimapp/books2.php");
        echo "editar";
    }catch(PDOException  $e){
        echo $e->getMessage();
    }
});
// Editar
$app->get("/edit", function () use($app) {
   
        $connection = getConnection();
        $dbh = $connection->prepare("SELECT * FROM books");
        $dbh->execute();
        $books = $dbh->fetch(7);
        
        echo json_encode($books);
        $nID=$books;
        $app->redirect("/app/slimapp/editar.php");
   
});


// borramos libros - DELETE
$app->post("/books/dele/", function() use($app)
{
      if (isset($_POST['submit'])) {
        //   if ( !empty($app->request->get['id'])) {
        // $id = $app->request->post('id');
        // }
        $id         = $app->request->post('id');
        
    }else{
            echo "error";
    }
    try{
        $connection = Database::getConnection();
        $dbh = $connection->prepare("DELETE FROM books WHERE id = ?");
        $dbh->bindParam(1, $id);
        $dbh->execute();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(' Se ha borrado el # '.$id);
        $app->redirect('/app/slimapp/books.php');
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
});

 function getId(){
        $connection = getConnection();
        $dbh = $connection->prepare("SELECT * FROM books");
        $dbh->execute();
        $books = $dbh->fetch(7);
        
        echo json_encode($books);
        var_export($books);
        // $nID=$books;
}

// corremos la aplicacion
$app->run();

// git remote add appmd3 git@bitbucket.org:jmorelos96/automotriz-firo.git
