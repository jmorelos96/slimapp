<?php
include 'index.php';
require "central.php";
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	</head>
	<body>
	    <div class="container">
	    	<div class="row">
	    		<h2>Libros</h2>
	    	</div>
			<div class="row">
				<p>
					<a href="form.php" class="btn btn-primary">Crear</a>
				</p>
				<table class="table table-hover table-bordered">
			        <thead>
			            <tr>
			                <th>Titulo</th>
			                <th>Autor</th>
			                <th>Resumen</th>
			                <th>Accion</th>
			            </tr>
			        </thead>
			        <tbody>
			            <?php 
							$pdo = Database::getConnection();
							$sql = 'SELECT * FROM books ORDER BY id Asc';
			 				foreach ($pdo->query($sql) as $row) {
								   	echo '<tr height=5>';
									echo '<td>'. $row['title'] . '</td>';
									echo '<td>'. $row['author'] . '</td>';
									echo '<td>'. $row['summary'] . '</td>';
									echo '<td width=180 height=5>';
									echo '<a class="btn btn-primary" href="update.php?id='.$row['id'].'">Editar</a>';
									echo '&nbsp;';
									echo '<a class="btn btn-danger" href="delete.php?id='.$row['id'].'">Borrar</a>';
									echo '</td>';
									echo '</tr>';
						   }
						  ?>
					</tbody>
		        </table>
	    	</div>
	    </div> <!-- /container -->
	</body>
</html>