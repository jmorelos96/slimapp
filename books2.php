<?php
require "index.php";

require "central.php";


$app->get("/books", function ($id) use($app) {
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("SELECT * FROM books");
        $dbh->execute();
        $books = $dbh->fetchAll();
        $app->response()->header("Content-Type", "application/json");
        echo json_encode($books);
    }catch(PDOException  $e){
        echo $e->getMessage();
    }
});
$app->get("/book/:id", function($id) use($app) {
   try{
        $connection = getConnection();
        $dbh = $connection->prepare("SELECT * FROM books WHERE id = ?");
        $dbh->bindParam(1, $id);
        $dbh->execute();
        $book = $dbh->fetch();
        $connection = null;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->body(json_encode($book));
    }
    catch(PDOException $e)
    {
        echo "Error: " . $e->getMessage();
    }
});
// obteniendo el id
$connection = getConnection();
        $dbh = $connection->prepare("SELECT * FROM books");
        $dbh->execute();
        $books = $dbh->fetch(7);
        
        echo json_encode($books);

echo '<a href="./edit">Lista de libros</a><br>';

?>
<div class="col-md-4">
<a href="/slimapp/books">Lista de libros</a><br>
<!-- 
<a href='/slimapp/book/2'> Libro 2 </a>
<a href='/slimapp/book/3'> Libro 3 </a> -->
<!-- <a href="/slimapp/book/1">libro #1</a> -->
<br>

	
</div>