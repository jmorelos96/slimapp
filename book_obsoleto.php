<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>Lista de libros</title>
    <link rel="stylesheet" href="../../jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="../../styles/bootstrap.min.css" type="text/css" />
    <script type="text/javascript" src="../../scripts/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="../../jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxgrid.pager.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxgrid.sort.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxgrid.edit.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxgrid.filter.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxgrid.columnsresize.js"></script>
    <script type="text/javascript" src="../../jqwidgets/jqxgrid.selection.js"></script> 
    <script type="text/javascript" src="../../jqwidgets/jqxpanel.js"></script>
   <script type="text/javascript" src="../../scripts/demos.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var url = "/app/slimapp/books/";
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'id', type: 'string' },
                    { name: 'title', type: 'string' },
                    { name: 'author', type: 'int' },
                    { name: 'summary', type: 'string' },
                    // { name: 'Status', type: 'string' }
                ],
                id:    'id',
                url:   url,
                type: 'GET'
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#jqxgrid").jqxGrid(
            {
                width: 700,
                source: dataAdapter,
                selectionmode: 'multiplerowsextended',
                sortable: true,
                altrows: true,
                // pageable: true,
                editable: true,
                everpresentrowposition: "top",
                height: 400,
                columnsresize: true,
                // pagermode: 'simple',
                columns: [
                  { text: 'id', datafield: 'id', width: 50 },
                  { text: 'titulo', datafield: 'title', width: 220 },
                  { text: 'autor', datafield: 'author', width: 210 },
                  { text: 'resumen', datafield: 'summary', width: 220 },
                  // { text: 'Estado', datafield: 'Status', width: 60}
              ]
            });
            
        });
    </script>
</head>
<body class='default'>
    <center>  
        <h1>Libros</h1>
            <a href="form.php">Crear </a>|
            <a href="update.php"> Editar </a>|
            <a href="delete.php"> Borrar </a>
     
            <div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: center; ">
                
                <div id="jqxgrid" >
                </div>

            </div>
        
        
       

    </center>    
</body>
</html>